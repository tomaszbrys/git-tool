
#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <stdio.h>
#include <stdlib.h>

//---------------------------------------------------------------------
static long asub_func(aSubRecord *precord) {
    
    static unsigned counter = 0;

    // definition of variables, int and float
    int   val1      = *(int*)precord->a;
    float val2      = *(float*)precord->b;
    char  *s        = (char*)precord->c;

    // definition of array and nr of elements in the array
    float *array1   = (float *)precord->d;  
    int   narray1   = precord->ned;

    // implementation  
    counter++;

    //-
    *(int *)precord->vala = counter;
    printf("counter:%u \n", counter);
   
    //-
    *(float *)precord->valb = sin(val2);
    printf("val2:%f, sin(val2)=%f \n", val2, sin(val2));
    
    //-
    for(size_t i = 0; i < narray1; i++){
       array1[i] = array1[i] + 10; 
    }
    *(float *)array1 = array1;

return 0;
}


epicsRegisterFunction(asub_func);
