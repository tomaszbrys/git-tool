#!/bin/bash

set -e
set -u

########## VARIABLES ###########
# GITTOOLPATH="/Users/tomaszbrys/Documents/vagrant_share/git-tools"
# GITTOOLPATH="/vagrant/git-tools"
REPONAME=$(basename `pwd`)
REPOPATH=https://gitlab.esss.lu.se/tomaszbrys
# REPOPATH=https://gitlab.esss.lu.se/icshwi/nss-instruments
ASYNDRIVER=0
STREAMDEVICE=0
ASUBFUNC=0
VERBOSE=0
WRAPPER=${REPONAME}
MODULE=${REPONAME:3}

#repo_path=https://gitlab.esss.lu.se/icshwi/nss-instruments


#--------------------------------------------------------------------
function usage()
{
   cat << HEREDOC
   ${HOSTNAME}
   Usage: git-tool [optional arguments]

   optional arguments:
     -h, --help           show this help message and exit
     -a, --asynDriver     create asynDriver empty module    
     -s, --streamDevice   create streamDevice empty module
     -f, --aSub           create aSub record in the module
     -l, --snl            create state notation language in the module
     -r, --remote-repo    name of remote repository on gitlab
     -R, --show-repo      show the default remote repository 
     -p, --push           push e3-wrapper and module to remote gitlab repository
     -v, --verbose        increase the verbosity of the bash script

HEREDOC
}  

#--------------------------------------------------------------------
function updateMakefile()
{
echo "update Makefile"
}
#--------------------------------------------------------------------
function createStreamDevice()
{
echo "[DEBUG] function create StreamDevice"

echo "[DEBUG] copy templates"

#---------------------------------------------------------------------
# First info.txt
cat ${GITTOOLPATH}/TEMPLATES/info.txt    \
    | sed -e "s/<M>/${MODULE}/"          \
          -e "s/<A>/$(whoami)/"          \
          -e "s/<E>/$(whoami)@ess.eu/"   \
          -e "s/<D>/$(date)/"            \
          -e "1,9s/./&#/69"              \
    > ${MODULE}/${MODULE}App/Db/${MODULE}.template
# then template
cat ${GITTOOLPATH}/TEMPLATES/temp_db.template \
    >> ${MODULE}/${MODULE}App/Db/${MODULE}.template

#---------------------------------------------------------------------
# proto file, first info
cat ${GITTOOLPATH}/TEMPLATES/info.txt    \
    | sed -e "s/<M>/${MODULE}/"          \
          -e "s/<A>/$(whoami)/"          \
          -e "s/<E>/$(whoami)@ess.eu/"   \
          -e "s/<D>/$(date)/"            \
          -e "1,9s/./&#/69"              \
    > ${MODULE}/${MODULE}App/Db/${MODULE}.proto
# then proto file template
cat ${GITTOOLPATH}/TEMPLATES/temp_stream.proto \
    >> ${MODULE}/${MODULE}App/Db/${MODULE}.proto

#---------------------------------------------------------------------
# update Makefile
echo "[DEBUG]  Makefile, uncomment REQUIRED in ${MODULE}.Makefile"
echo "[DEBUG]  Makefile, uncomment TEMPLATES"

sed -i '/^# REQUIRED += stream/s/^#//;' ${MODULE}.Makefile
sed -i '/^# EXCLUDE_ARCHS/s/^#//' ${MODULE}.Makefile
sed -i '/^#APP:=/s/^#//' ${MODULE}.Makefile
sed -i '/^#APPDB:=/s/^#//' ${MODULE}.Makefile
sed -i '/^#APPSRC:=/s/^#//' ${MODULE}.Makefile
sed -i '/^#APPCMDS:=/s/^#//' ${MODULE}.Makefile
sed -i '/^# TEMPLATES/s/^#//g' ${MODULE}.Makefile

# add stream_VERSION=$(STREAM_DEP_VERSION) after this line
STREAM_DEP_VERSION=2.8.10
echo "[DEBUG]  Makefile, add _DEP_VERSION ${STREAM_DEP_VERSION}"
sed -i "s/REQUIRED += stream/&\nstream_VERSION=${STREAM_DEP_VERSION}/" ${MODULE}.Makefile 

#---------------------------------------------------------------------
# update configure/CONFIG_MODULE
echo "[DEBUG]  configure/CONFIG_MODULE, add stream VERSION"
sed -i "s/^# DEPENDENT MODULE VERSION/&\nSTREAM_DEP_VERSION=${STREAM_DEP_VERSION}/" configure/CONFIG_MODULE 
        
#---------------------------------------------------------------------
# create cmds
cat ${GITTOOLPATH}/TEMPLATES/temp.cmd    \
    | sed -e "s/<N>/${MODULE}/g"         \
    > cmds/${MODULE}.cmd

#---------------------------------------------------------------------
# create iocsh
cat ${GITTOOLPATH}/TEMPLATES/temp.iocsh  \
    | sed -e "s/<N>/${MODULE}/g"         \
    > ${MODULE}/iocsh/${MODULE}.iocsh

#---------------------------------------------------------------------
# copy template OPI
mkdir -p ${MODULE}/opi
cat ${GITTOOLPATH}/TEMPLATES/temp.bob    \
    | sed -e "s/Display/${MODULE}/"          \
          -e "s/>Title</>${MODULE}</"          \
    > ${MODULE}/opi/${MODULE}.bob
#

########## OSX #######################################
#sed -i '' 's/# REQUIRED += stream/REQUIRED += stream/g' ${MODULE}.Makefile
#sed -i '' '/REQUIRED += stream/s/^#/ /g' ${MODULE}.Makefile
#sed -i '' '/EXCLUDE_ARCHS/s/^#/ /g'      ${MODULE}.Makefile
#sed -i '' '/APP:=/s/^#/ /g'              ${MODULE}.Makefile
#sed -i '' '/APPDB:=/s/^#/ /g'            ${MODULE}.Makefile
#sed -i '' '/APPSRC:=/s/^#/ /g'           ${MODULE}.Makefile
#sed -i '' '/APPCMDS:=/s/^#/ /g'          ${MODULE}.Makefile
#sed -i '' '/TEMPLATES/s/^#/ /g'          ${MODULE}.Makefile
#echo "[DEBUG]  Makefile, add _DEP_VERSION ${STREAM_DEP_VERSION}"
#sed -i ''  -e '/REQUIRED += stream/ {a\ 
#    'stream_VERSION=${STREAM_DEP_VERSION}'
#    }' ${MODULE}.Makefile
#sed -i ''  -e '/^# DEPENDENT MODULE VERSION/ {a\ 
#    'STREAM_DEP_VERSION:=${STREAM_DEP_VERSION}'
#    }' configure/CONFIG_MODULE
######### end OSX ######################################


}
#--------------------------------------------------------------------
function createASub()
{

echo "[DEBUG] function createASub"

echo "[DEBUG] copy templates"
cat ${GITTOOLPATH}/TEMPLATES/temp_aSub.c \
    | sed -e "s/<M>/${MODULE}/"          \
          -e "s/<A>/$(whoami)/"          \
          -e "s/<E>/$(whoami)@ess.eu/"   \
          -e "s/<D>/$(date)/"            \
          -e "1,9s/./&#/69"              \
    > ${MODULE}/${MODULE}App/src/${MODULE}.c

echo "[DEBUG] copy Db"
cat ${GITTOOLPATH}/TEMPLATES/temp_aSub.template \
    | sed -e "s/<M>/${MODULE}/"          \
          -e "s/<A>/$(whoami)/"          \
          -e "s/<E>/$(whoami)@ess.eu/"   \
          -e "s/<D>/$(date)/"            \
          -e "1,9s/./&#/69"              \
    >> ${MODULE}/${MODULE}App/Db/${MODULE}.template

echo "[DEBUG] copy dbd"
cat ${GITTOOLPATH}/TEMPLATES/temp_aSub.dbd \
    > ${MODULE}/${MODULE}App/src/${MODULE}.dbd
    

# 2. 
# add aSub record to Db with input, output, string, waveform, int, float, short
# 3. 
# add dbd file in module/moduleApp/src
# 4.
# modify makefile 
}
#--------------------------------------------------------------------
function createAsynDriver()
{
echo "create asynDrriver"
}
#--------------------------------------------------------------------



function pushRepo2()
{
echo "[DEBUG] function pushRepo2 to remote repo"
#url=https://gitlab.esss.lu.se/tomaszbrys/e3-ami1700
echo "3xDUPA"
url=${REPOPATH}/${WRAPPER}
#if [ "$?" -eq 0 ]; then
if git ls-remote ${url} > /dev/null 2>&1; then
    echo "[DEBUG] EXISTS"
else     
    echo "[DEBUG] DOES NOT"
fi


}

#------------------------------------------------------
function pushRepo()
{
url=${REPOPATH}/${WRAPPER}
#url=https://gitlab.esss.lu.se/tomaszbrys/e3-ami1700

echo "[DEBUG] function push to remote repo"
echo "[DEBUG] remote repository link: ${url}"
# check if remote repository exists

#if [ "$?" -eq 0 ]; then
if git ls-remote ${url} > /dev/null 2>&1; then
  echo "[DEBUG] Remote repository exists"
  cd ${MODULE}
  git add .
  git commit -m"Update"
  git push
  cd .. 
  git add .
  git commit -m"Update"
  git push
else
   echo "[DEBUG] Remote repository does not exist"
   echo "[DEBUG] Create remote repository"
   # go to the driver directory (assuming you are in e3-${repo_name})
   echo "[DEBUG] cd ${MODULE}"
   cd ${MODULE}

   ### first you have to initialise the ddd repository, create remote repo and push it.
   echo "[DEBUG] git init"
   git init

   echo "[DEBUG] git remote add origin ${REPOPATH}/${MODULE}.git"
   git remote add origin ${REPOPATH}/${MODULE}.git

   echo "[DEBUG] git add ."
   git add .

   echo "[DEBUG] git commit -m Initial Commit ${MODULE}"
   git commit -m"initial commit ${MODULE}"

   echo "[DEBUG] git push --set-upstream ${REPOPATH}/${MODULE}.git master"
   git push --set-upstream ${REPOPATH}/${MODULE}.git master
   # check if the remote repository was created in the gitlab, it is private, change it to public

   # Now we have to add ${repo_name} as submodule to e3-${repo_name}
   # go to the e3-${repo_name} directory, assuming we are {repo_name} directory
   echo "[DEBUG] cd .. to e3-${MODULE}"
   cd .. 

   echo "[DEBUG] git submodule add ${REPOPATH}/${MODULE}.git"
   git submodule add ${REPOPATH}/${MODULE}.git

   echo "[DEBUG] git remote add origin ${REPOPATH}/${WRAPPER}.git"
   git remote add origin ${REPOPATH}/${WRAPPER}.git

   echo "[DEBUG] git add ."
   git add .

   echo "[DEBUG] git commit ${WRAPPER}"
   git commit -m"initial commit ${WRAPPER}"

   echo "[DEBUG] git push -u origin master"
   git push -u origin master
   # check if the remote repository was created in the gitlab, it is private, change it to public
fi   
}
#--------------------------------------------------------------------
function showRepo()
{
echo "remote repository is set to:"
echo "   repository: $REPOPATH"
echo "   e3-wrapper: $REPOPATH/$WRAPPER"
echo "   module    : $REPOPATH/$WRAPPER/$MODULE"
}
#--------------------------------------------------------------------


#####################################################################
#########  Start script  ############################################
#####################################################################

OPTS=$(getopt -o "hasfr:Rpv" --long "help,asynDriver,streamDevice,aSub,remote-repo,show-repo,push,verbose" -a -- "$@")
if [ $? != 0 ] ; then echo "Error in command line arguments." >&2 ; usage; exit 1 ; fi
eval set -- "$OPTS"


while true; do
  case "$1" in
    -h | --help )           usage;              exit; ;;
    -p | --push )           pushRepo;          shift ;;
    -a | --asynDriver )     createAsynDriver;   shift ;;
    -s | --streamDevice )   createStreamDevice; shift ;;
    -f | --aSub )           createASub;         shift ;;
    -r | --remote-repo )    pushRepo;           shift ;;
    -R | --show-repo )      showRepo;           shift ;;
    -v | --verbose )        VERBOSE=1;          shift ;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done



if (( $VERBOSE > 0 )); then

   # print out all the parameters we read in
cat <<EOM
   verbose      : $VERBOSE
   repo name    : $REPONAME
   repo path    : $REPOPATH
   wrapper      : $WRAPPER
   module       : $MODULE
   asyn driver  : $ASYNDRIVER
   stream device: $STREAMDEVICE
   aSub         : $ASUBFUNC
EOM
fi

