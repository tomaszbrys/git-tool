Bash script to help create and manage E3 modules 
   Usage: git-tool [optional arguments]

   optional arguments:

     -h, --help           show this help message and exit
   
     -a, --asynDriver     create asynDriver empty module    
   
     -s, --streamDevice   create streamDevice empty module
   
     -f, --aSub           create aSub record in the module
   
     -l, --snl            create state notation language in the module
   
     -r, --remote-repo    name of remote repository on gitlab
   
     -R, --show-repo      show the default remote repository 
   
     -p, --push           push e3-wrapper and module to remote gitlab repository
   
     -v, --verbose        increase the verbosity of the bash script

